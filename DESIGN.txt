COMMANDS TO IMPLEMENT

API 0.1 - Often-used commands, part I: easy commands
----------------------------------------------------

X git add
X git commit
X git init
X git mv
X git rm
X git checkout
X git clone
X git branch
X git revert
X git reset
X git clean
X git gc
git status - It takes the same arguments 'git commit' does, so we only need to copy the signatures. Where to add this method? In Git::Repository? Git::Repository::Branch? Add a new Git::Repository::WorkingTree class?
git remote

API 0.2 - Often-used commands, part II: difficult commands
----------------------------------------------------------

X git merge
X git push - BEWARE! Split in Git::Repository::push (for 'git-push repo') and Git::Repository::Branch::push (for 'git-push --repo=repo')
X git fetch
X git pull
X git rebase
X git config
git diff

API 0.3 - Not-that-often-used commands, part I: easy commands
-------------------------------------------------------------

git archive - Using fuse-zip, libarchive or zziplib might be needed (see http://people.freebsd.org/~kientzle/libarchive/ and http://apps.sourceforge.net/mediawiki/fuse/index.php?title=CompressedFileSystems and http://apps.sourceforge.net/mediawiki/fuse/index.php?title=ArchiveFileSystems and http://apps.sourceforge.net/mediawiki/fuse/index.php?title=VersioningFileSystems#gitfs libzzip.sf.net ) 
git describe
git show
git stash - We need a Stash class (Git::Repository::Stash or Git::Repository::Branch::Stash ? )
git tag
git prune (is it needed or is git gc enough?)
X git update-server-info
git fsck

API 0.4 - Not-that-often-used commands, part II: difficult commands
-------------------------------------------------------------------

git log
git grep
git bisect
git submodule
git blame

API 0.5 - Special commands
--------------------------

git svn
git send email
githooks (install, remove, etc)

==============================================


Git::Config --> Config object. There should be one per repository (access with Git::Config Git::Repository::config() ), one per system (static Git::Config Git::config() ). How to access the user's git config? 

Git::Repository --> Each repository
         |- Git::Repository(URL) -> if URL contains a git repository, we are done. If URL does not contain a git repository, init() would create one.
         |- bool init()
         |- QStringList add( ... files ...) -> return list of files added
         |- XXX commit(message) -> git commit -m "message" -> is it necessary to return anything? bool?
         |- commit(message, Git::All) -> git commit -a -m "message" -> is it necessary to return anything? bool?
         |- config() -> return a Git::Config object. Is there a config per repository or a config per branch? Check git docs
         |- clone( Git::Repository existent_repo )
         |- gc()
         |- log()
         |- reset()
         |- diff(), diff(id1, id2)
         |- status()
         |- fetch()
         |- merge( Git::Repository::Branch, Git::Repository::Branch)
         |- rebase()
         |- pull()
         |- push()
         |- tag()
         |- QStringList branch(), bool branch()
         |- checkout()
         |- revert()
         |- show(id)
         |- fsck
	 |- rm
	 |- mv

Git::Repository::Branch --> Branches (maybe Git::Branch?)

Git::Commit -> commit id's (sha1 hash)

