#include "qtgit.h"

namespace Git {

    /**
      * Convenience method: checkout this branch and add a file to the index
      * git add [--ignore-errors] [--refresh] files
      */
    const Result Repository::Branch::add ( const QStringList& files, const Repository::Index::AddFlags f ) {
// TODO
    }

    /**
      * Convenience method: checkout this branch and add a file to the index
      * git add [--ignore-errors] [--refresh] files
      */
    const Result Repository::Branch::add ( const QList<QUrl*> files, const Repository::Index::AddFlags f ) {
// TODO
    }

    /**
      * Convenience method: checkout this branch and add a file to the index
      * git add [--ignore-errors] [--refresh] files
      */
    const Result Repository::Branch::add ( const QList<QFile*> files, const Repository::Index::AddFlags f ) {
// TODO
    }

    /**
      * Convenience method: checkout this branch and update the index
      * git add [--ignore-errors] [--refresh] -u
      */
    const Result Repository::Branch::update ( const Repository::Index::AddFlags f ) {
// TODO
    }

    /**
      * Convenience method: checkout this branch and add all (known) files to the index
      * git add [--ignore-errors] [--refresh] -A
      */
    const Result Repository::Branch::addAll ( const Repository::Index::AddFlags f ) {
// TODO
    }


/// git mv [ -f ] [ -k ] orig dest
    const Result Repository::Branch::rename ( const QFile* orig,
                                      const QFile* dest,
                                      const RenameFileFlags f ) {
// TODO
    }

/// Convenience method - Same as above (git mv [ -f ] [ -k ] orig dest)
    const Result Repository::Branch::rename ( const QUrl* orig,
                                      const QUrl* dest,
                                      const RenameFileFlags f ) {
// TODO
    }

// From git-rm

    quint32 Repository::Branch::remove ( const QList<QFile*> files, const FileRemoveFlags f ) {
// TODO
    }

    quint32 Repository::Branch::remove ( const QList<QUrl*> files, const FileRemoveFlags f ) {
// TODO
    }

// From git-branch

/// abbrev = 0 => git branch --no-abbrev; abbrev = othervalue => git branch --abbrev=othervalue
    QList<Repository::Branch*> Repository::Branch::list ( quint8 abbrev,
            const BranchListMode f ) const {
// TODO
    }

/// abbrev = 0 => git branch --no-abbrev; abbrev = othervalue => git branch --abbrev=othervalue
    QList<Repository::Branch*> Repository::Branch::list ( const Repository::Commit* startingPoint,
            const BranchListCommitMode m,
            quint8 abbrev,
            const BranchListMode f ) const {
// TODO
    }

    /**
      * Lightweight constructor which points to an already-existing branch.
      * This constructor is private and has Git::Repository as a friend-class, as
      * it probably only makes sense to use it from Repository::currentBranch()
      */
    Repository::Branch::Branch ( const QString& name, Git::Repository* repo ) : m_repository(repo), m_name(name) {
      ;
    }

    /**
    * Branches branch 'existingbranch' into a branch called 'newbranchname'
    * git branch [options] newbranchname existingbranch
    * f = Default => git branch existingbranch newbranch; f = Force => git branch -f existingbranch newbranch
    */
    Repository::Branch::Branch ( Repository::Branch* existingbranch,
                                 const QString& newbranchname,
                                 const BranchCreateFlags f,
                                 const TrackMode m ) {
// TODO next
    }

    /**
      * Branches the currently-checked-out branch into a branch called 'newbranchname'
      * git branch [options] newbranchname
      *
      * This is a convenience method which does the same as:
      * \code Git::Repository::Branch( repo->currentBranch(), "newbranchname" ); \endcode
      *
      * f = Default => git branch existingbranch newbranch; f = Force => git branch -f existingbranch newbranch
      */
    Repository::Branch::Branch ( const QString& newbranchname,
                                 const BranchCreateFlags f,
                                 const TrackMode m ) {
// TODO
    }

    /**
      * Delete a branch
      * f = Default => git branch -d branch; f = Force => git branch -D branch
      */
    const Result Repository::Branch::remove ( const BranchRemoveFlags f ) {
// TODO
    }

    /**
      * Rename a branch
      * f = Default => git branch -m oldbranchanme newbranchname; f = Force => git branch -M oldbranchname newbranchname
      */
    const Result Repository::Branch::rename ( const QString& newbranchname,
                                      const BranchRenameFlags f ) {
// TODO
    }

// From git-reset

/// git-reset [--mixed | --soft | --hard] [<commit>] => 'commit' will be the new head
    const Result Repository::Branch::reset ( Commit* commit, ResetMode rm ) {
// TODO
    }

/// git reset [-q] [<commit>] [--] <paths> => revert paths in 'paths' from commit 'commit'
    const Result Repository::Branch::reset ( Commit* commit, QList<QUrl> paths ) {
// TODO
    }

/// git reset [-q] [<commit>] [--] <paths> => revert paths in 'paths' from commit 'commit'
    const Result Repository::Branch::reset ( Commit* commit, QList<QFile> paths ) {
// TODO
    }

/// This is a convenience method you can use when you want to merge only one branch. It's equivalent to "merge( QList<Branch>() << mybranch, "", RecursiveMergeStrategy )"
    const Result Repository::Branch::merge ( const Branch* remote,
                                            const QString& message,
                                            MergeStrategy strategy,
                                            LogCommitDescriptionMode lcd,
                                            FastForwardMergeMode ffmm,
                                            ReturnDiffStatMode s,
                                            SquashMergeMode sm ) {
// TODO
    }

/// Use this method when you want to merge more than one branch
    const Result Repository::Branch::merge ( QList<Branch> remotes,
                                            const QString& message,
                                            MergeStrategy strategy,
                                            LogCommitDescriptionMode lcd,
                                            FastForwardMergeMode ffmm,
                                            ReturnDiffStatMode s,
                                            SquashMergeMode sm ) {
// TODO
    }

// From git-push

    /** This method is equivalent to:
      *   git push [--all | --mirror] [--dry-run] [--tags]
      *            [--receive-pack=<git-receive-pack>] [--repo=<repository>]
      *            [-f | --force] [-v | --verbose]
      *
      * The method equivalent to:
      *   git push [--all | --mirror] [--dry-run] [--tags]
      *            [--receive-pack=<git-receive-pack>]  [-f | --force]
      *            [-v | --verbose] [<repository> <refspec>...]
      * is in the Git::Repository class, as it has effect on a repository
      */
    const Result Repository::Branch::push ( Repository* repo,
                                      const QStringList& refspecs,
                                      RefPushMode rpm,
                                      PushFlags pf,
                                      PushThinMode ptm,
                                      const QString& git_receive_pack ) {
// TODO
    }


// From git-rebase

    /** git rebase [-m | --merge] [-s <strategy> | --strategy=<strategy>]
      *           [-C<n>] [ --whitespace=<option>]
      *           <upstream>
      */
    const Result Repository::Branch::rebase ( Commit* upstream,
                                        const PullMode pm,
                                        MergeStrategy strategy,
                                        quint32 context,
                                        CleanUpMode cum ) {
// TODO
    }

    /** git rebase [-m | --merge] [-s <strategy> | --strategy=<strategy>]
      *           [-C<n>] [ --whitespace=<option>]
      *           [--onto <newbase>] <upstream>
      */
    const Result Repository::Branch::rebase ( Commit* upstream,
                                        Commit* onto,
                                        const PullMode pm,
                                        MergeStrategy strategy,
                                        quint32 context,
                                        CleanUpMode cum ) {
// TODO
    }

/// git-rebase --continue | --skip | --abort
    const Result Repository::Branch::rebase ( ResolveMergeConflictMode rmcm ) {
// TODO
    }

/// This branch's HEAD
    Repository::Commit Repository::Branch::head() {
// TODO
    }

};
