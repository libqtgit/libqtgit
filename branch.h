/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_BRANCH_
#define _H_BRANCH_

#include "repository.h"
#include "index.h"

#include <QObject>
#include <QUrl>
// #include <QDir>
#include <QFile>
// #include <QString>
// #include <QList>
#include <QStringList>
// #include <climits>
// #include <QRegExp>
#include <climits>

namespace Git {

    /**
     * \brief A class representing a branch of a repository.
     *
     * Because of the way libQtGit is currently implemented (QProcess
     * which runs 'git'), a repository may have infinite branches but
     * you can only work with one branch at the same time.
     *
     * Please note this limitation does not mean you cannot have two
     * Git::Branch objects and work with them at the same time (you can
     * and it will work fine) but you cannot have two threads accessing
     * two branches of the same repository at the same time.
     *
     * Let's say you have repositories R1 and R2. Repository R1 has branches
     * R1_B1 and R1_B2. Repository R2 as branches R2_B1 and R2_B2.
     *
     * What you can do:
     *  - Create as much branches as you want in R1 and R2, in a single thread
     *    or in different threads
     *  - Commit, revert, reset, etc (operations which imply modifying the index,
     *    configuration or any other detail in the state of the repository) as
     *    much times as you want to R1_B1 OR R1_B2 from one thread and R2_B1
     *    OR R2_B2 from another, different thread
     *  - Status, diff, log, etc (operations which do NOT modify the index,
     *    configuration or any other detail in the state of the repository) as
     *    much times as you want to R1_B1, R1_B2, R2_B1 AND R2_B2 at the very
     *    same time, from the same or different threads
     *
     * What you can NOT do (at least, not safely):
     *  - Commit, revert, reset, etc (operations which imply modifying the index,
     *    configuration or any other detail in the state of the repository) at
     *    the same time to R1_B1 AND R1_B2, OR R2_B1 AND R2_B2, from two different threads
     *
     * So in summary, what you CANNOT do is writing to the same repository from two threads at
     * the same time because in the improbable, but possible, case both QProcesses write at
     * the same time, you might corrupt the repository. Setting the 'core.sharedRepository'
     * config option might help but I wouldn't bet on that.
     *
     */

    class Repository::Branch : public QObject {

            friend Git::Repository::Branch* Git::Repository::currentBranch();

            Q_OBJECT

        public:

            // From git-add

            /// Convenience methods: these methods checkout the branch and call the Git::Repository::Index corresponding method
            const Result add ( const QStringList& files, const Repository::Index::AddFlags f = Repository::Index::DefaultAddFlags );

            /**
             * Convenience method: checkout this branch and add a file to the index
             * git add [--ignore-errors] [--refresh] files
             */
            const Result add ( const QList<QUrl*> files, const Repository::Index::AddFlags f = Repository::Index::DefaultAddFlags );

            /**
             * Convenience method: checkout this branch and add a file to the index
             * git add [--ignore-errors] [--refresh] files
             */
            const Result add ( const QList<QFile*> files, const Repository::Index::AddFlags f = Repository::Index::DefaultAddFlags );

            /**
             * Convenience method: checkout this branch and update the index
             * git add [--ignore-errors] [--refresh] -u
             */
            const Result update ( const Repository::Index::AddFlags f = Repository::Index::DefaultAddFlags );

            /**
             * Convenience method: checkout this branch and add all (known) files to the index
             * git add [--ignore-errors] [--refresh] -A
             */
            const Result addAll ( const Repository::Index::AddFlags f = Repository::Index::DefaultAddFlags );


            // TODO Provide convenience methods for committing (like the ones we provide for adding)

            // From git-mv

            enum RenameFileFlag {
                DefaultRenameFlags = 0x0 /*< git mv orig dest */,
                ForceRename = 0x1 /*< git mv -f orig dest */,
                SkipErrorsWhileRenaming = 0x2 /*< git mv -k orig dest */
            };
            Q_DECLARE_FLAGS ( RenameFileFlags, RenameFileFlag )

            /// git mv [ -f ] [ -k ] orig dest
            const Result rename ( const QFile* orig,
                          const QFile* dest,
                          const RenameFileFlags f = Repository::Branch::DefaultRenameFlags );

            /// Convenience method - Same as above (git mv [ -f ] [ -k ] orig dest)
            const Result rename ( const QUrl* orig,
                          const QUrl* dest,
                          const RenameFileFlags f = Repository::Branch::DefaultRenameFlags );

            // From git-rm

            enum FileRemoveFlag {
                DefaultRemoveFlags = 0x0 /*< git rm file */,
                ForceRemove = 0x01 /*< git rm -f file */,
                RecursiveRemove = 0x2 /*< git rm -r dir */,
                CachedRemove = 0x4 /*< git rm --cached file -- DANGER: I DON'T REALLY UNDERSTAND WHAT --cached DOES */
            };
            Q_DECLARE_FLAGS ( FileRemoveFlags, FileRemoveFlag )

            quint32 remove ( const QList<QFile*> files, const FileRemoveFlags f );
            quint32 remove ( const QList<QUrl*> files, const FileRemoveFlags f );


            enum BranchRemoveFlag {
                DefaultBranchRemoveFlags = 0x0 /*< 'git branch existingbranch newbranch' and 'git branch -d branch' */,
                ForceRemoveBranch = 0x1 /*< 'git branch -f existingbranch newbranch' and 'git branch -D oldbranchname newbranchname' */,
                RemoveRemoteBranch = 0x2 /*< 'git branch -d -r branch' and 'git branch -D -r branch' */
            };
            Q_DECLARE_FLAGS ( BranchRemoveFlags, BranchRemoveFlag )

            enum BranchCreateFlag {
                DefaultBranchCreateFlags = 0x0 /*< git branch branchname [startingpoint] */,
                ForceCreateBranch = 0x1 /*< git branch -f branchname [startingpoint] */,
                CreateRemoteBranch = 0x2 /*< git branch -r branchname [startingpoint] */
            };
            Q_DECLARE_FLAGS ( BranchCreateFlags, BranchCreateFlag )

            enum BranchRenameFlag {
                DefaultBranchRenameFlags = 0x0 /*< 'git branch existingbranch newbranch', 'git branch -d branch' and 'git branch - oldbranchname newbranchname' */,
                ForceBranchRename = 0x1 /*< 'git branch -f existingbranch newbranch', 'git branch -m branch' and 'git branch -M oldbranchname newbranchname' */,
            };
            Q_DECLARE_FLAGS ( BranchRenameFlags, BranchRenameFlag )

            enum BranchListMode {
                DefaultBranchListBehavior = 0x0 /*< git branch */,
                ListLocalBranches = 0x0 /*< git branch - Same as DefaultBranchListBehavior, but it is conveniente to have a name */,
                ListRemoteBranches = 0x1 /*< git branch -r */,
                ListAllBranches = 0x2 /*< git branch -a */
            };
            Q_ENUMS ( BranchListMode )

            enum BranchListCommitMode {
                ListMergedCommit = 0x0 /*< git branch [-r | -a] [--abbrev=abbrev] --merged [commit] */,
                ListNotMergedCommit = 0x1 /*< git branch [-r | -a] [--abbrev=abbrev] --no-merged [commit] */,
                ListContainedCommit = 0x2 /*< git branch [-r | -a] [--abbrev=abbrev] --contains [commit] */,
            };
            Q_ENUMS ( BranchListCommitMode )

            // From git-branch

            /// abbrev = 0 => git branch --no-abbrev; abbrev = othervalue => git branch --abbrev=othervalue
            QList<Repository::Branch*> list ( quint8 abbrev = 7,
                                              const BranchListMode f = ListLocalBranches ) const;

            /// abbrev = 0 => git branch --no-abbrev; abbrev = othervalue => git branch --abbrev=othervalue
            QList<Repository::Branch*> list ( const Repository::Commit* startingPoint,
                                              const BranchListCommitMode m,
                                              quint8 abbrev = 7,
                                              const BranchListMode f = ListLocalBranches ) const;


private:
            /**
             * Lightweight constructor which points to an already-existing branch.
             * This constructor is private and has Git::Repository as a friend-class, as
             * it probably only makes sense to use it from Repository::currentBranch()
             */
            Branch ( const QString& name, Git::Repository* repo );

public:
            /**
             * Branches branch 'existingbranch' into a branch called 'newbranchname'
             * f = Default => git branch existingbranch newbranch; f = Force => git branch -f existingbranch newbranch
             */
            Branch ( Repository::Branch* existingbranch,
                     const QString& newbranchname,
                     const BranchCreateFlags f = DefaultBranchCreateFlags,
                     const TrackMode m = DefaultTrackCheckoutBehavior );

            /**
             * Branches the currently-checked-out branch into a branch called 'newbranchname'
             *
             * This is a convenience method which does the same as:
             * \code Git::Repository::Branch( repo->currentBranch(), "newbranchname" ); \endcode
             *
             * f = Default => git branch existingbranch newbranch; f = Force => git branch -f existingbranch newbranch
             */
            Branch ( const QString& newbranchname,
                     const BranchCreateFlags f = DefaultBranchCreateFlags,
                     const TrackMode m = DefaultTrackCheckoutBehavior );

            /**
             * Delete a branch
             * f = Default => git branch -d branch; f = Force => git branch -D branch
             */
            const Result remove ( const BranchRemoveFlags f = DefaultBranchRemoveFlags );

            /**
             * Rename a branch
             * f = Default => git branch -m oldbranchanme newbranchname; f = Force => git branch -M oldbranchname newbranchname
             */
            const Result rename ( const QString& newbranchname,
                          const BranchRenameFlags f = DefaultBranchRenameFlags );

            // From git-reset

            /// \todo Should 'reset' be in Git::Repository::Branch or in Git::Repository?
            enum ResetMode {
                DefaultResetBehavior = 0x0 /*< git-reset commit */,
                MixedReset = 0x1 /*< git-reset --mixed commit */,
                SoftReset = 0x2 /*< git-reset --soft commit */,
                HardReset = 0x4 /*< git-reset --hard commit */
            };
            Q_ENUMS ( ResetMode )

            /// git-reset [--mixed | --soft | --hard] [<commit>] => 'commit' will be the new head
            const Result reset ( Commit* commit, ResetMode rm );

            /// git reset [-q] [<commit>] [--] <paths> => revert paths in 'paths' from commit 'commit'
            const Result reset ( Commit* commit, QList<QUrl> paths );

            /// git reset [-q] [<commit>] [--] <paths> => revert paths in 'paths' from commit 'commit'
            const Result reset ( Commit* commit, QList<QFile> paths );

            // WARNING! THE FOLLOWING CODE IS DUPLICATED IN Git::Repository FOR git-pull USE

            // From git-merge

            enum MergeStrategy {
                ResolveMergeStrategy = 0x0 /*< git-merge -s resolve */,
                RecursiveMergeStrategy = 0x1 /*< git-merge -s recursive */,
                OctopusMergeStrategy = 0x2 /*< git-merge -s octopus */,
                OursMergeStrategy = 0x4 /*< git-merge -s ours */,
                SubtreeMergeStrategy = 0x8 /*< git-merge -s subtree */
            };
            Q_ENUMS ( MergeStrategy )

            enum ReturnDiffStatMode {
                DefaultDiffStatBehavior = 0x0 /*< git-merge */,
                NoDiffStat = 0x1 /*< git-merge -n */,
                ReturnDiffStat = 0x2 /*< git-merge --stat */
            };
            Q_ENUMS ( ReturnMergeStatMode )

            enum LogCommitDescriptionMode {
                DefaultLogCommitDescriptionBehavior = 0x0 /*< git-merge */,
                NoLogCommitDescription = 0x1 /*< git-merge --no-log */,
                LogCommitDescription = 0x2 /*< git-merge --log */
            };
            Q_ENUMS ( LogCommitDescriptionMode )

            enum FastForwardMergeMode {
                DefaultMergeFastForwardBehavior = 0x0 /*< git-merge */,
                NoMergeFastForward = 0x1 /*< git-merge --no-ff */,
                MergeFastForward = 0x2 /*< git-merge --ff */
            };
            Q_ENUMS ( FastForwardMergeMode )

            enum SquashMergeMode {
                DefaultSquashMergeBehavior = 0x0 /*< git-merge */,
                SquashMerge = 0x1 /*< git-merge --squash */,
                NoSquashMerge = 0x2 /*< git-merge --no-squash */
            };
            Q_ENUMS ( SquashMergeMode )

            // END OF DUPLICATED CODE

            /// This is a convenience method you can use when you want to merge only one branch. It's equivalent to "merge( QList<Branch>() << mybranch, "", RecursiveMergeStrategy )"
            const Result merge ( const Branch* remote,
                                const QString& message = QString(),
                                MergeStrategy strategy = RecursiveMergeStrategy,
                                LogCommitDescriptionMode lcd = DefaultLogCommitDescriptionBehavior,
                                FastForwardMergeMode ffmm = DefaultMergeFastForwardBehavior,
                                ReturnDiffStatMode s = DefaultDiffStatBehavior,
                                SquashMergeMode sm = DefaultSquashMergeBehavior );

            /// Use this method when you want to merge more than one branch
            const Result merge ( QList<Branch> remotes,
                                const QString& message = QString(),
                                MergeStrategy strategy = OctopusMergeStrategy,
                                LogCommitDescriptionMode lcd = DefaultLogCommitDescriptionBehavior,
                                FastForwardMergeMode ffmm = DefaultMergeFastForwardBehavior,
                                ReturnDiffStatMode s = DefaultDiffStatBehavior,
                                SquashMergeMode sm = DefaultSquashMergeBehavior );

            // From git-push

            enum RefPushMode { /*< FIXME This is duplicated in Git::Repository - We should get rid of the duplication by removing it here! */
                DefaultRefPushBehavior = 0x0 /*< git-push repo */,
                MergeRefPushAll = 0x1 /*< git-push --all repo */,
                RefPushMirror = 0x2 /*< git-push --mirror repo */
            };
            Q_ENUMS ( RefPushMode )

            enum PushFlag { /*< FIXME This is duplicated in Git::Repository - We should get rid of the duplication by removing it here! */
                DefaultPushFlags = 0x0 /*< git-push repo */,
                PushTags = 0x1 /*< git-push --tags repo */,
                ForcePush = 0x2 /*< git-push --force repo */
            };
            Q_DECLARE_FLAGS ( PushFlags, PushFlag )

            enum PushThinMode { /*< FIXME This is duplicated in Git::Repository - We should get rid of the duplication by removing it here! */
                DefaultPushThinBehavior = 0x0 /*< git-push repo */,
                PushThin = 0x1 /*< git-push --thin repo */,
                NoPushThin = 0x2 /*< git-push --no-thin repo */
            };
            Q_ENUMS ( PushThinMode )


            /** This method is equivalent to:
             *   git push [--all | --mirror] [--dry-run] [--tags]
             *            [--receive-pack=<git-receive-pack>] [--repo=<repository>]
             *            [-f | --force] [-v | --verbose]
             *
             * The method equivalent to:
             *   git push [--all | --mirror] [--dry-run] [--tags]
             *            [--receive-pack=<git-receive-pack>]  [-f | --force]
             *            [-v | --verbose] [<repository> <refspec>...]
             * is in the Git::Repository class, as it has effect on a repository
             */
            const Result push ( Repository* repo,
                          const QStringList& refspecs = QStringList(),
                          RefPushMode rpm = DefaultRefPushBehavior,
                          PushFlags pf = DefaultPushFlags,
                          PushThinMode ptm = DefaultPushThinBehavior,
                          const QString& git_receive_pack = QString() );


            // From git-rebase

            enum ResolveMergeConflictMode {
                ResolveConflictByContinuing = 0x0 /*< git-rebase --continue */,
                ResolveConflictBySkipping = 0x1 /*< git-rebase --skip */,
                ResolveConflictByAborting = 0x2 /*< git-rebase --abort */
            };
            Q_ENUMS ( ResolveMergeConflictMode )

            /** git rebase [-m | --merge] [-s <strategy> | --strategy=<strategy>]
             *           [-C<n>] [ --whitespace=<option>]
             *           <upstream>
             */
            const Result rebase ( Commit* upstream /*< We need the Commit::Commit(Branch* b) constructor for this to work */,
                            const PullMode pm = DefaultPullBehavior,
                            MergeStrategy strategy = RecursiveMergeStrategy /*< This parameter only has effect if PullMode = MergePull */,
                            quint32 context = ULONG_MAX,
                            CleanUpMode cum = DefaultCleanUpBehavior );

            /** git rebase [-m | --merge] [-s <strategy> | --strategy=<strategy>]
             *           [-C<n>] [ --whitespace=<option>]
             *           [--onto <newbase>] <upstream>
             */
            const Result rebase ( Commit* upstream /*< We need the Commit::Commit(Branch* b) constructor for this to work */,
                            Commit* onto /*< We need the Commit::Commit(Branch* b) constructor for this to work */,
                            const PullMode pm = DefaultPullBehavior,
                            MergeStrategy strategy = RecursiveMergeStrategy /*< This parameter only has effect if PullMode = MergePull */,
                            quint32 context = ULONG_MAX,
                            CleanUpMode cum = DefaultCleanUpBehavior );

            // This signature will never exist, as it breaks object-oriented
            // programming good practices -- See the FAQ
            //
            // git rebase [-m | --merge] [-s <strategy> | --strategy=<strategy>]
            //           [-C<n>] [ --whitespace=<option>]
            //           <upstream> [<branch>]
            // Result rebase( Commit* upstream, /* We need the Commit::Commit(Branch* b) constructor for this to work */
            //      Commit* workingBranch, /* We need the Commit::Commit(Branch* b) constructor for this to work */
            //      const PullMode = DefaultPullBehavior,
            //      MergeStrategy strategy = RecursiveMergeStrategy, /* This parameter only has effect if PullMode = MergePull */
            //      quint32 context = ULONG_MAX,
            //      CleanUpMode = DefaultCleanUpBehavior );

            // This signature will never exist, as it breaks object-oriented
            // programming good practices -- See the FAQ
            //
            // git rebase [-m | --merge] [-s <strategy> | --strategy=<strategy>]
            //           [-C<n>] [ --whitespace=<option>]
            //           [--onto <newbase>] <upstream> [<branch>]
            // Result rebase( Commit* upstream, /* We need the Commit::Commit(Branch* b) constructor for this to work */
            //      Commit* onto, /* We need the Commit::Commit(Branch* b) constructor for this to work */
            //      Commit* workingBranch, /* We need the Commit::Commit(Branch* b) constructor for this to work */
            //      const PullMode = DefaultPullBehavior,
            //      MergeStrategy strategy = RecursiveMergeStrategy, /* This parameter only has effect if PullMode = MergePull */
            //      quint32 context = ULONG_MAX,
            //      CleanUpMode = DefaultCleanUpBehavior );

            /// git-rebase --continue | --skip | --abort
            const Result rebase ( ResolveMergeConflictMode rmcm );

            /// This branch's HEAD
            Commit head();

        private:
            Git::Repository* m_repository; /// The repository this branch is a branch of
            QString m_name; /// The branch name, as stored by git. TODO I'm not sure if git accepts special characters here; if it does not, it'd probably make sense to use a QLatin1String here
    };

    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Branch::RenameFileFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Branch::FileRemoveFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Branch::BranchRemoveFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Branch::BranchCreateFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Branch::BranchRenameFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Branch::PushFlags )
}

#endif
