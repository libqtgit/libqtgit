#include "qtgit.h"

namespace Git {

    Repository::Commit::Commit ( Repository* repo ) {
// TODO
    }

    /// This constructor creates a commit object from a hash, human-readable id or any other "tree-ish" reference. It's useful in case you want to get more information or examine that object.
    Repository::Commit::Commit( const QString& treeish ) {
           // TODO

    }

    // returns "sha1^"
    Repository::Commit::Commit parent() {
        // TODO
    }

    // returns "sha1~level"
    Repository::Commit ancestor( quint32 level ) {
        // TODO
    }

/// if message = QString() => git-revert --no-edit
    bool Repository::Commit::revert ( const QString& message,
                                      RevertFlags rf ) {
// TODO
    }

/// if message = QString() => git-revert --no-edit
    bool Repository::Commit::revert ( quint32 parent,
                                      const QString& message,
                                      RevertFlags rf ) {
// TODO
    }



    /** From git-commit
      *
      * These methods do the same as:
      * Git::Repository::Branch( ..., Git::Repository::Branch::AmendCommit, ...)
      * The Git::Repository::Branch::AmendCommit is unconditionally added to the CommitFlags
      * passed by the user of these methods.
      */

/// git commit -C commit_id -m "message"
    const Result Repository::Commit::amend ( const QString& message,
                                       const QString& author,
                                       CommitFlags cf,
                                       CleanUpMode cum ) {
// TODO
    }

/// git-commit -F f -C commit_id
    const Result Repository::Commit::amend ( const QUrl* f,
                                       const QString& author,
                                       CommitFlags cf,
                                       CleanUpMode cum ) {
// TODO
    }

/// Convenience method - Same as above (git-commit -F f -C commit_id)
    const Result Repository::Commit::amend ( const QFile* f,
                                       const QString& author,
                                       CommitFlags cf,
                                       CleanUpMode cum ) {
// TODO
    }

    const QString& Repository::Commit::id() const {
        return m_commitId;
    }

    const QString& Repository::Commit::shortId() const {
        return m_commitShortId;
    }

    void Repository::Commit::setId ( const QString& id ) {
// TODO
    }

};
