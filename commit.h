/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_COMMIT_
#define _H_COMMIT_

#include <QObject>
#include <QUrl>
#include <QFile>
#include <QString>

//\todo TODO It should be Git::Repository::Commit, not Git::Repository::Branch::Commit !!! This requires changes in Git::Repository::Branch, too
namespace Git {

    /**
     * \brief A class representing a commit in a repository
     */
    class Repository::Commit : public QObject {

            Q_OBJECT

        public:
            /// This constructor returns the branch's HEAD and is needed for git-rebase to accept both commits and branches as the <newbase>, <upstream> and <branch> parameters
            Commit ( Repository* repo );

            /// This constructor creates a commit object from a hash, human-readable id or any other "tree-ish" reference. It's useful in case you want to get more information or examine that object.
            Commit( const QString& treeish );

            // returns "sha1^"
            Commit parent();

            // returns "sha1~level"
            Commit ancestor( quint32 level );

            // From git-revert

            enum RevertFlag {
                DefaultRevertBehavior = 0x0 /*< git revert [--edit | --no-edit] [-m parent-number] <commit> */,
                NoCommitRevert = 0x1 /*< git revert [--edit | --no-edit] -n [-m parent-number] <commit> */,
                SignOffRevert = 0x2 /*< git revert [--edit | --no-edit] [-m parent-number] -s <commit> */
            };
            Q_DECLARE_FLAGS ( RevertFlags, RevertFlag )

            /** We cannot merge both signatures for 'revert' in a single one using parent = 1 :
             * \verbatim
             <pgquiles_> are "git revert commit" and "git revert -m 1 commit" the same thing ?
             <Pieter> pgquiles_: for non-merge commits probably yes
             <Pieter> pgquiles_: for merge-commits, the first fails and the second does something \endverbatim
             */

            /// if message = QString() => git-revert --no-edit
            bool revert ( const QString& message = QString(),
                          RevertFlags rf = DefaultRevertBehavior );

            /// if message = QString() => git-revert --no-edit
            bool revert ( quint32 parent,
                          const QString& message = QString(),
                          RevertFlags rf = DefaultRevertBehavior );



            /** From git-commit
             *
             * These methods do the same as:
             * Git::Repository::Branch( ..., Git::Repository::Branch::AmendCommit, ...)
             * The Git::Repository::Branch::AmendCommit is unconditionally added to the CommitFlags
             * passed by the user of these methods.
             */

            /// git commit -C commit_id -m "message"
            const Result amend ( const QString& message = QString(),
                           const QString& author = QString(),
                           CommitFlags cf = DefaultCommitFlags,
                           CleanUpMode cum = DefaultCleanUpBehavior );

            /// git-commit -F f -C commit_id
            const Result amend ( const QUrl* f,
                           const QString& author = QString(),
                           CommitFlags cf = DefaultCommitFlags,
                           CleanUpMode cum = DefaultCleanUpBehavior );

            /// Convenience method - Same as above (git-commit -F f -C commit_id)
            const Result amend ( const QFile* f,
                           const QString& author = QString(),
                           CommitFlags cf = DefaultCommitFlags,
                           CleanUpMode cum = DefaultCleanUpBehavior );

            const QString& id() const;
            const QString& shortId() const;

        private:
            void setId ( const QString& id );
            QString m_commitId; /// SHA-1 hash for this commit
            QString m_commitShortId; /// A part of the SHA-1 hash for this commit

    };
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Commit::RevertFlags )

}

#endif
