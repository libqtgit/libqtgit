/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_CONFIG_
#define _H_CONFIG_

#include "repository.h"
#include <QObject>
#include <QUrl>
#include <QFile>
#include <QString>
#include <QList>
#include <QVariant>
#include <QStringList>
#include <QRegExp>

/** Namespace for Git-related stuff in libQtGit */
namespace Git {

    /**
     * \brief Base class for git configuration settings. Do NOT use this class, it will eventually become private.
     */
    class ConfigBase : public QObject {

            Q_OBJECT

        public:
            /// Default constructor
            ConfigBase();

            /// git config name [value [value_regex]]
            bool set ( const QString& name, const QString& value = QString(), const QRegExp& value_regex = QRegExp() );

            /// git config --int name [value [value_regex]]
            bool set ( const QString& name, const quint32 value, const QRegExp& value_regex = QRegExp() );

            /// git config --bool name [value [value_regex]]
            bool set ( const QString& name, const bool value, const QRegExp& value_regex = QRegExp() );



            /// git config --add name value
            bool add ( const QString& name, const QString& value );

            /// git config --int --add name value
            bool add ( const QString& name, const quint32 value );

            /// git config --bool --add name value
            bool add ( const QString& name, const bool value );




            /// git config --replace-all name
            bool replaceAll ( const QString& name );

            /// git config --replace-all name [value [value_regex]]
            bool replaceAll ( const QString& name, const QString& value, const QRegExp& value_regex = QRegExp() );

            /// git config --int --replace-all name [value [value_regex]]
            bool replaceAll ( const QString& name, const quint32 value, const QRegExp& value_regex = QRegExp() );

            /// git config --bool --replace-all name [value [value_regex]]
            bool replaceAll ( const QString& name, const bool value, const QRegExp& value_regex = QRegExp() );



            /// git config --get name [value_regex]
            /// "name" must be variable name, for regexp's use Git::Config::get(const QRegExp&)
            QString get ( const QString& name, const QRegExp& value_regex = QRegExp() ) const;

            /// git config --int --get name [value_regex]
            quint32 getInt ( const QString& name, const QRegExp& value_regex = QRegExp() ) const;

            /// git config --bool --get name [value_regex]
            bool getBool ( const QString& name, const QRegExp& value_regex = QRegExp() ) const;

            /// git config --get-regexp name_regex [value_regex]
            QString get ( const QRegExp& name_regex, const QRegExp& value_regex = QRegExp() ) const;

            /// git config --int --get-regexp name_regex [value_regex]
            quint32 getInt ( const QRegExp& name_regex, const QRegExp& value_regex = QRegExp() ) const;

            /// git config --bool --get-regexp name_regex [value_regex]
            bool getBool ( const QRegExp& name_regex, const QRegExp& value_regex = QRegExp() ) const;



            /// git config --get-all name [value_regex]
            QList<QVariant> getAll ( const QString& name, const QRegExp& value_regex = QRegExp() ) const;

            /** git config --get-all name [value_regex]
             * This is a convenience method which does the same
             * "QList<QVariant> getAll(const QString& name, const QRegExp& value_regex = QRegExp() ) const;"
             * does but returns all the values as a list of QString's instead of QVariant's
             */
            QStringList getAllAsString ( const QString& name, const QRegExp& value_regex = QRegExp() ) const;



            /// git config --unset name [value_regex]
            bool unset ( const QString& name, const QRegExp& value = QRegExp() );



            /// git config --unset-all name [value_regex]
            bool unsetAll ( const QString& name, const QRegExp& value = QRegExp() );



            /// git config --rename-section old_name new_name
            bool renameSection ( const QString& old_name, const QString& new_name );



            /// git config --remove-section name
            bool removeSection ( const QString& name );



            /// git config --list
            QList<QVariant> list();

            /** git config --list
             * This is a convenience method which does the same
             * "QList<QVariant> list();" does but returns all the values as a
             * list of QString's instead of a a list of QVariant's
             */
            QStringList listAsString();

        protected:
            void setConfigFile ( const QString& file );
            void setConfigFile ( QUrl* file );
            void setConfigFile ( QFile* file );

        private:
            QString m_configFile;

    };

}



namespace Git {

    /**
     * \brief System-wide (global) and user-specified file config files
     */
    class Config : public Git::ConfigBase {

            Q_OBJECT

        public:
            /// System-wide config: $(prefix)/etc/gitconfig
            Config();

            /// Support for 'git-config -f file'
            Config ( QUrl* file );

            /// Convenience method for 'git-config -f file'
            Config ( QFile* file );


    };

}


namespace Git {

    /**
     * \brief A class representing a repository config (i. e. the \$GIT_DIRECTORY/.git/config file )
     */
    class Repository::Config : public Git::ConfigBase {

            Q_OBJECT

        public:
            Config();

    };

}

#endif
