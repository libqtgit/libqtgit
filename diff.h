/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_DIFF_
#define _H_DIFF_

#include <QObject>

namespace Git {
    /// Are Patch and Diff the same thing?
    class Diff : public QObject {

            Q_OBJECT

    };

}

#endif
