#include <QtGit>
#include <iostream>

int main ( int argc, char* argv[] ) {
    if ( argc != 2 ) {
        std::cout << "This utility adds a file to the index\n" << std::endl;
        std::cout << "Usage: " << argv[0] << "file\n" << std::endl;
        return 1;
    }

    QFileInfo fileinfo(argv[1]);
    if( !fileinfo.exists() ) {
        std::cerr << "Error: file " << argv[1] << " does not exist\n";
        return 1;
    }

    QString path( fileinfo.absolutePath() );
    if( path.isEmpty() ) {
        std::cerr << "Error: invalid path\n";
        return 1;
    }

    Git::Repository repo ( path, Git::Repository::ExistingRepository );
    if( repo.path().isNull() ) {
        std::cerr << "Error: construction of repository object failed\n";
        return 1;
    }

    Git::Repository::Index idx(&repo);

    idx.add( QStringList() << argv[1] );
    // TODO Check return value from idx.add(...) to make sure it works fine (it's currently impossible because Git::Result and its children are not yet implemented :-)

    return 0;
}
