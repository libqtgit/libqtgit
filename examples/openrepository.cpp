#include <QtGit>
#include <iostream>

int main ( int argc, char* argv[] ) {
    if ( argc != 2 ) {
        std::cout << "This utility opens a repository in the specified path\n" << std::endl;
        std::cout << "Usage: argv[0] path\n" << std::endl;
        return 1;
    }

    QString repopath ( argv[1] );
    std::cout << "Trying to open the repository that was in " << qPrintable ( repopath ) << std::endl;
    Git::Repository repo ( repopath, Git::Repository::ExistingRepository );
    std::cout << "Opened the repository that was in " << qPrintable ( repo.path() ) << std::endl;
    return ( repo.path() != QString() ? 0 : 1 );
}
