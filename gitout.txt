## GIT: COMMANDS, EXIT CODES, STDOUT OUTPUT AND STDERR OUTPUT

## This document is organized like this:
##
## GIT COMMAND
##
## desc: description
## -
## commands needed to prepare the test
## -
## git command --args ...
## -
## exit code
## -
## stderr
## -
## stdout
## -
## comments:
## -
##
## exit code, stderr and stdout are obtained by running 'git command --args ... 2>>err 1>>out ; echo $?'
##
## NOBOT means the output/error/whatever is dependant on the input or other conditions and cannot be automated
##
## TODO: Add test cases where the disk is full and trying to add something to the index is not possible because there is not a single byte free

GIT INIT

desc: create repository in an empty, writable directory
-
mkdir repo; cd repo
-
git init
-
0
-
(empty)
-
Initialized empty Git repository in /home/pgquiles/tmp/gittests/repo/.git/
-
comments:
-

desc: reinitialize repository in a writable directory
-
mkdir repo; cd repo; git init
-
git init
-
0
-
(empty)
-
Reinitialized existing Git repository in /home/pgquiles/tmp/gittests/repo/.git/
-
comments:
-

desc: create bare repository in an empty, writable directory
-
mkdir repo; cd repo
-
git init --bare
-
0
-
(empty)
-
Initialized empty Git repository in /home/pgquiles/tmp/gittests/repo/
-
comments:
-

desc: reinitialize bare repository in a writable directory which contains an already-existing non-bare repository
-
mkdir repo; cd repo; git init
-
git init --bare
-
0
-
(empty)
-
Initialized empty Git repository in /home/pgquiles/tmp/gittests/repo/
-
comments: it does not reinitialize the repository but create new repository. If a bare and a non-bare repository are available, git prefers to use the non-bare (i. e. 'git command ...' writes to .git/..., not to the bare repository)
-

desc: reinitialize bare repository in a writabel director which contains an already-existing bare repository
-
mkdir repo; cd repo; git init --bare
-
git init --bare
-
0
-
(empty)
-
Reinitialized existing Git repository in /home/pgquiles/tmp/gittests/repo/
-
comments:
-

desc: create repository in a non-writeable directory
-
mkdir repo; sudo chown -R root.root repo; cd repo; touch err out; sudo chown pgquiles.pgquiles err out
-
git init
-
1
-
/home/pgquiles/tmp/gittests/repo/.git: Permission denied
-
(empty)
-
comments:
-

GIT ADD

desc: add file to index (file exists)
-
mkdir repo; cd repo; git init; touch hello.txt
-
git add hello.txt
-
0
-
(empty)
-
(empty)
-
comments:
-

desc: add file to index (file does not exist)
-
mkdir repo; cd repo; git init
-
git add hello.txt
-
128
-
fatal: pathspec 'hello.txt' did not match any files
-
(empty)
-
comments:
-

desc: add file to index (file exists, it was already added to the index and there are no changes since the last commit)
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt; git commit -m "Blah"
-
git add hello.txt
-
0
-
(empty)
-
(empty)
-
comments:
-

desc: add file to index (file exists but user has no read permission on the file)
-
mkdir repo; cd repo; git init; touch hello.txt; chmod 000 hello.txt
-
git add hello.txt
-
128
-
error: open("hello.txt"): Permission denied
error: unable to index file hello.txt
fatal: adding files failed
-
(empty)
-
comments:
-

desc: add file to index (file exists but user has no write permission on the .git directory)
-
mkdir repo; cd repo; git init; touch hello.txt; sudo chown -R root.root .git
-
git add hello.txt
-
128
-
fatal: unable to create '.git/index.lock': Permission denied
-
(empty)
-
comments:
-

desc: add file to index with --ignore-errors (file does not exist)
-
mkdir repo; cd repo; git init
-
git add --ignore-errors hello.txt
-
128
-
fatal: pathspec 'hello.txt' did not match any files
-
(empty)
-
comments:
-

desc: add file to index with --ignore-errors (file exists but user has no read permission on the file)
-
mkdir repo; cd repo; git init; touch hello.txt; chmod 000 hello.txt
-
git add --ignore-errors hello.txt
-
1
-
error: open("hello.txt"): Permission denied
error: unable to index file hello.txt
-
(empty)
-
comments:
-

desc: add file to index with --ignore-errors (file exists but user has no write permission on the .git directory)
-
mkdir repo; cd repo; git init; touch hello.txt; sudo chown -R root.root .git
-
git add --ignore-errors hello.txt
-
128
-
fatal: unable to create '.git/index.lock': Permission denied
-
(empty)
-
comments:
-

desc: update index (there are no changes in known files since last commit)
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt; git commit -m "blah"
-
git add -u
-
0
-
(empty)
-
(empty)
-
comments:
-

desc: update index (there are changes in known files since last commit)
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt; git commit -m "blah"; echo "hello" >> hello.txt
-
git add -u
-
0
-
(empty)
-
(empty)
-
comments:
-

desc: update index (user has no read permission on a known file)
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt; git commit -m "blah"; echo "hello" >> hello.txt; chmod 000 hello.txt
-
git add -u
-
128
-
error: open("hello.txt"): Permission denied
error: unable to index file hello.txt
fatal: updating files failed
-
(empty)
-
comments:
-

desc: update index (user has no write permission on the .git directory)
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt; git commit -m "blah"; echo "hello" >> hello.txt; sudo chown -R root.root .git
-
git add -u
-
128
-
fatal: unable to create '.git/index.lock': Permission denied
-
(empty)
-
comments:
-


desc: find current checkout out branch
-
mkdir repo; cd repo; git init
-
git symbolic-ref HEAD
-
0
-
(empty)
-
refs/heads/master
-
comments: The output depends on the checked out branch (for instance, if the checked out branch was 'cmake', it would return "refs/heads/cmake")
-

desc: find current checkout out branch (no git repository exists)
-
mkdir repo; cd repo
-
git symbolic-ref HEAD
-
128
-
fatal: Not a git repository (or any of the parent directories): .git
-
(empty)
-
comments:
-

GIT COMMIT

desc: commit with previously added file(s), permissions OK, done in a git directory, first commit ever
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt
-
git commit -m "Add file hello.txt"
-
0
-
(empty)
-
[master (root-commit) ebe09dc] Add file hello.txt
 0 files changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 hello.txt
-
comments:
-

desc: commit with previously added file(s), permissions OK, done in a git directory, not first commit ever
-
mkdir repo; cd repo; git init; touch hello.txt; git add hello.txt; git commit -m "Add file hello.txt"; echo "blah" >> hello.txt; git add hello.txt
-
git commit -m "Second commit"
-
0
-
(empty)
-
[master 3e23646] Second commit
 1 files changed, 1 insertions(+), 0 deletions(-)
-
comments:
-

desc: commit with nothing to commit, permissions OK, done in a git directory
-
mkdir repo; cd repo; git init; touch hello.txt
-
git commit -m "Add file hello.txt"
-
1
-
(empty)
-
# On branch master
#
# Initial commit
#
# Untracked files:
#   (use "git add <file>..." to include in what will be committed)
#
#       err
#       hello.txt
#       out
nothing added to commit but untracked files present (use "git add" to track)
-
comments:
-

GIT BRANCH

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-

desc: description
-
commands needed to prepare the test
-
git command --args ...
-
exit code
-
stderr
-
stdout
-
comments:
-
