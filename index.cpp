#include "qtgit.h"
#include <QProcess>

namespace Git {

    /** Returns the index for repository 'repo'
      * DANGER! When you execute methods on an index, they are executed on the
      * branch which is checked out at that moment. Either you make the checked
      * out branch is the one you want to work with, or you use the convenience
      * methods Git::Branch::add,  Git::Branch::update, etc (those methods
      * check out the branch, then run the Git::Repository::Index methods)
      */
    Repository::Index::Index ( const Repository* const repo ) : m_repository ( repo ) {
        ;
    }

    /**
      * Add a file to the index
      * git add [--ignore-errors] [--refresh] files
      */
    const Result Repository::Index::add ( const QStringList& files, const Repository::Index::AddFlags f ) {
// TODO Parse output to detect if files exist, if we have permissions do write to the index, etc

        QStringList args;

        args << QLatin1String("add");

        if ( f & Repository::Index::IgnoreErrorsInIndex ) {
            args << QLatin1String ( "--ignore-errors" );
        }

        if ( f & Repository::Index::RefreshIndex ) {
            args << QLatin1String ( "--refresh" );
        }

        // Files to pass to 'git add'
        args << files;

        QProcess gitp;
        gitp.setWorkingDirectory ( m_repository->m_path );
        gitp.start ( QLatin1String ( "git" ), args );
        if ( !gitp.waitForStarted() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "Cannot start git command line utility" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( !gitp.waitForFinished() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "The git command line utility blocked" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( gitp.exitCode() != 0 ) {
            return Git::ErrorResult ( gitp.exitCode(), QStringList() << QString ( gitp.readAllStandardError() ) );
        }

        return Git::InformativeResult ( 0, QStringList() << QLatin1String ( "" ) );
    }

    /**
      * Add a file to the index
      * git add [--ignore-errors] [--refresh] files
      */
    const Result Repository::Index::add ( const QList<QUrl*> files, const Index::AddFlags f ) {
// TODO
    }

    /**
      * Add a file to the index
      * git add [--ignore-errors] [--refresh] files
      */
    const Result Repository::Index::add ( const QList<QFile*> files, const Index::AddFlags f ) {
// TODO
    }

    /**
      * Update the index
      * git add [--ignore-errors] [--refresh] -u
      */
    const Result Repository::Index::update ( const Index::AddFlags f ) {
        QStringList args;
        args << QLatin1String("add");

        if ( f & Repository::Index::IgnoreErrorsInIndex ) {
            args << QLatin1String ( "--ignore-errors" );
        }

        if ( f & Repository::Index::RefreshIndex ) {
            args << QLatin1String ( "--refresh" );
        }

        args << QLatin1String ( "-u" );

        QProcess gitp;
        gitp.setWorkingDirectory ( m_repository->m_path );
        gitp.start ( QLatin1String ( "git" ), args );
        if ( !gitp.waitForStarted() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "Cannot start git command line utility" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( !gitp.waitForFinished() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "The git command line utility blocked" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( gitp.exitCode() != 0 ) {
            return Git::ErrorResult ( gitp.exitCode(), QStringList() << QString ( gitp.readAllStandardError() ) );
        }

        return Git::InformativeResult ( 0, QStringList() << QLatin1String ( "" ) );
    }

    /**
      * Add all files to the index
      * git add [--ignore-errors] [--refresh] -A
      */
    const Result Repository::Index::addAll ( const Index::AddFlags f ) {
        QStringList args;
        args << QLatin1String("add");

        if ( f & Repository::Index::IgnoreErrorsInIndex ) {
            args << QLatin1String ( "--ignore-errors" );
        }

        if ( f & Repository::Index::RefreshIndex ) {
            args << QLatin1String ( "--refresh" );
        }

        // Files to pass to 'git add'
        args << QLatin1String ( "-A" );

        QProcess gitp;
        gitp.setWorkingDirectory ( m_repository->m_path );
        gitp.start ( QLatin1String ( "git" ), args );
        if ( !gitp.waitForStarted() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "Cannot start git command line utility" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( !gitp.waitForFinished() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "The git command line utility blocked" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( gitp.exitCode() != 0 ) {
            return Git::ErrorResult ( gitp.exitCode(), QStringList() << QString ( gitp.readAllStandardError() ) );
        }

        return Git::InformativeResult ( 0, QStringList() << QLatin1String ( "" ) );
    }

};
