/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_INDEX_
#define _H_INDEX_

#include <QObject>
#include <QUrl>
#include <QFile>
#include <QStringList>
#include <QList>

namespace Git {

    /**
     * \brief A class representing the index of a repository
     *
     * DANGER! When you execute methods on an index, they are executed on the
     * branch which is checked out at that moment. Either you make the checked
     * out branch is the one you want to work with, or you use the convenience
     * methods Git::Branch::add,  Git::Branch::update, etc (those methods
     * check out the branch, then run the Git::Repository::Index methods)
     */
    class Repository::Index : public QObject {

            Q_OBJECT

        public:

            /// TODO Add methods: QStringList stagedFileNames(...) const, QStringList unstagedFileNames(...) const

            enum AddFlag {
                DefaultAddFlags = 0x0 /*< git add */,
                IgnoreErrorsInIndex = 0x1 /*< git add --ignore-errors */,
                RefreshIndex = 0x2 /*< git add --refresh */
            };
            Q_DECLARE_FLAGS ( AddFlags, AddFlag )

            /** Returns the index for repository 'repo'
              * DANGER! When you execute methods on an index, they are executed on the
              * branch which is checked out at that moment. Either you make the checked
              * out branch is the one you want to work with, or you use the convenience
              * methods Git::Branch::add,  Git::Branch::update, etc (those methods
              * check out the branch, then run the Git::Repository::Index methods)
              */
            Index ( const Repository* const repo );
            /**
             * Add a file to the index
             * git add [--ignore-errors] [--refresh] files
             */
            const Result add ( const QStringList& files, const Repository::Index::AddFlags f = DefaultAddFlags );

            /**
             * Add a file to the index
             * git add [--ignore-errors] [--refresh] files
             */
            const Result add ( const QList<QUrl*> files, const Repository::Index::AddFlags f = DefaultAddFlags );

            /**
             * Add a file to the index
             * git add [--ignore-errors] [--refresh] files
             */
            const Result add ( const QList<QFile*> files, const Repository::Index::AddFlags f = DefaultAddFlags );

            /**
             * Update the index
             * git add [--ignore-errors] [--refresh] -u
             */
            const Result update ( const Repository::Index::AddFlags f = DefaultAddFlags );

            /**
             * Add all files to the index
             * git add [--ignore-errors] [--refresh] -A
             */
            const Result addAll ( const Repository::Index::AddFlags f = DefaultAddFlags );

        private:
            const Repository* const m_repository; /// The repository this index belongs to
    };
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::Index::AddFlags )

}

#endif
