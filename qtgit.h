/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

/**
 * \mainpage libQtGit documentation
 * \section Introduction
 * Thank you for your interest in libQtGit. libQtGit is a C++ library
 * which provides a Qt-like API for embedding the git DVCS in your
 * application.
 * Documentation is yet scarce
 * \section FAQ
 * \include FAQ
 * \file
 * \author Pau Garcia i Quiles
 */

#ifndef _H_QTGIT_
#define _H_QTGIT_

#include "config.h"
#include "repository.h"
#include "index.h"
#include "commit.h"
#include "branch.h"
#include "result.h"
#include "patch.h"
#include "diff.h"

#endif
