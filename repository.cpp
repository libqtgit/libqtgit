#include "qtgit.h"
#include <QProcess>
#include <QTextStream>

namespace Git {

    const QString& Repository::path() const {
        return m_path;
    }

/// git commit -C commit_id -m "message"
    const Result Repository::commit ( const QString& message,
            const Commit* c,
            const QString& author,
            const CommitFlags cf,
            const CleanUpMode cum ) {
	return commit(StringMessage,new QFile(this),QString(), c,author,cf,cum);
    }

/// git commit -m "message"
/// This is a convenience method which does the same thing as 
/// Repository::commit( const QString& message, NULL, const QString& author, const CommitFlags cf, const CleanUpMode cum )
    const Result Repository::commit ( const QString& message,
            const QString& author,
            const CommitFlags cf,
            const CleanUpMode cum ) {
      return commit(message, 0, author, cf, cum);
    }

/// git commit -F f -C commit_id
/// This method takes the commit message from file 'f'
    const Result Repository::commit ( const QUrl* u,
            const Commit* c,
            const QString& author,
            const CommitFlags cf,
            const CleanUpMode cum ) {
      QFile f(u->toLocalFile());
      return commit(FileMessage,&f,QString(),c,author,cf,cum);
    }

/// Convenience method - Same as above (git commit -F f -C commit_id)
/// This method takes the commit message from file 'f'
    const Result Repository::commit ( const QFile* f,
            const Commit* c,
            const QString& author,
            const CommitFlags cf,
            const CleanUpMode cum ) {
      return commit(FileMessage,f,QString(),c,author,cf,cum);
    }

    const Result Repository::commit( CommitMode m,
	    const QFile* f,
	    const QString& message,
	    const Commit* c,
	    const QString& author,
	    const CommitFlags cf,
	    const CleanUpMode cum ) {
        QStringList args;
	args << QLatin1String("commit");
	switch(m) {
	  case FileMessage:
	    args << QLatin1String("-F") << QFileInfo(*f).absoluteFilePath();
	    break;
	  case StringMessage:
	    args << QLatin1String("-m") << message;
	    break;
	}
	  

        if ( cf & Repository::OverrideIndexCommit ) {
            args << QLatin1String ( "-a" );
        }

        if ( cf & Repository::SignOffCommit ) {
            args << QLatin1String ( "-s" );
        }

        if ( cf & Repository::AmendCommit ) {
            args << QLatin1String ( "--amend" );
        }

        if ( cf & Repository::AllowEmptyCommit ) {
            args << QLatin1String ( "--allow-empty" );
        }

        if ( cf & Repository::NoVerifyCommit ) {
            args << QLatin1String ( "-n" );
        }

        // If reuse commit message
        if( c ) {
            args << QLatin1String("-C") << c->id();
        }

        switch( cum ) {
            case DefaultCleanUpBehavior:
                break;
            case VerbatimClean:
                args << QLatin1String("--cleanup=verbatim");
                break;
            case WhiteSpaceClean:
                args << QLatin1String("--cleanup=whitespace");
                break;
            case StripClean:
                args << QLatin1String("--cleanup=strip");
                break;
            case DefaultClean:
                args << QLatin1String("--cleanup=default");
                break;
        }

        QProcess gitp;
        gitp.setWorkingDirectory ( m_path );
        gitp.start ( QLatin1String ( "git" ), args );
        if ( !gitp.waitForStarted() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "Cannot start git command line utility" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
//            return NULL; // TODO This is horrible! In case the method call fails, the user of Repository::commit gets no useful information. How to solve this?
        }

        if ( !gitp.waitForFinished() ) {
            return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "The git command line utility blocked" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
//            return NULL; // TODO This is horrible! In case the method call fails, the user of Repository::commit gets no useful information. How to solve this?
        }

        if ( gitp.exitCode() != 0 ) {
            return Git::ErrorResult ( gitp.exitCode(), QStringList() << QString ( gitp.readAllStandardError() ) );
//            return NULL; // TODO This is horrible! In case the method call fails, the user of Repository::commit gets no useful information. How to solve this?
        }

        // QProcess returns a QByteArray. Let's convert it to a QStringList.
        QTextStream stream( gitp.readAllStandardOutput() );
        QStringList out;

        QString line( stream.readLine() );
        while( !line.isNull() ) {
            out << line;
            line = stream.readLine();
        }

        return Git::InformativeResult( 0, QStringList() << QLatin1String ( "" ), out ); // TODO I do not like very much this return value, as it requires the user to parse for the hash of the commit if he wants to construct a Git::Commit object later on.
    }


// From git-init

/// 'dir' is where the git repository will be created
    Repository::Repository ( const QString& dir,
                             const OpenFlags mode ) /* : m_path ( dir ) */ {
        initCreate(dir,mode);
    }

    void Repository::initCreate(const QString& dir, const OpenFlags mode) {

        // Check if directory exists
        m_path = ( dir.isEmpty() ? QString::fromLatin1 ( "." ) : dir );

        if ( mode & Repository::ExistingRepository ) {
            // No parameters needed

            // Run 'git branch' to make sure there is a valid repository
            // If there is not, fail

            // Run 'git init args'
            QProcess gitp;
            gitp.setWorkingDirectory ( dir );
            gitp.start ( QLatin1String ( "git" ), QStringList() << QLatin1String ( "branch" ) );
            if ( !gitp.waitForStarted() ) {
                m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
            }

            if ( !gitp.waitForFinished() ) {
                m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
            }

            if ( gitp.exitCode() != 0 ) {
                m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
            }

        }

        QStringList args;
        args << "init";

        // Do we want a bare repository?
        if ( mode & Repository::BareRepository ) {
            args << QLatin1String ( "--bare" );
        }

        if ( mode & Repository::CreateRepository ) {
            // Run 'git init args'
            QProcess gitp;
            gitp.setWorkingDirectory ( dir );
            gitp.start ( QLatin1String ( "git" ), args );
            if ( !gitp.waitForStarted() ) {
                m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
            }

            if ( !gitp.waitForFinished() ) {
                m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
            }

            if ( gitp.exitCode() != 0 ) {
                m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
            }
        }

        // If we arrived here, we have a valid Git::Repository object

    }

/// 'dir' is where the git repository will be created
    Repository::Repository ( const QUrl* dir,
                             const OpenFlags mode ) {
	initCreate(dir->toLocalFile(),mode);
    }

/// 'dir' is where the git repository will be created
    Repository::Repository ( const QDir* dir,
                             const OpenFlags mode ) {
	initCreate(dir->absolutePath(),mode);
    }


// From git-clone

/// 'repo' is the repository we will clone, 'dir' is where it will be cloned in, 'origin' is for git clone --origin name
    Repository::Repository ( const Repository* repo,
                             const QString& dir,
                             const CloneFlags cf,
                             const QString& origin ) {
        initClone(repo,dir,cf,origin);
    }
    void Repository::initClone(const Repository* repo,
                               const QString& dir,
                               const CloneFlags cf,
                               const QString& origin) {
        m_path=dir;
        QStringList args;
        args << "clone" ;
        if(cf & CloneLocally) {
            args << "-l";
        }
        if(cf & CloneWithoutHardLinks) {
            args << "--no-hardlinks";
        }
        if(cf & CloneWithoutCheckOut) {
            args << "-n";
        }
        if(cf & BareClone) {
            args << "-b";
        }
        if(cf & CloneAndMirror) {
            args << "-m";
        }
        if(!origin.isEmpty() && !origin.isNull()) {
            args << "--origin" << origin;
        }
        args << repo->path();
        QProcess gitp;
        gitp.setWorkingDirectory ( dir );
        gitp.start ( QLatin1String ( "git" ), args );
        if ( !gitp.waitForStarted() ) {
            m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
        }

        if ( !gitp.waitForFinished() ) {
            m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
        }

        if ( gitp.exitCode() != 0 ) {
            m_path = QString(); // FIXME For now m_path = QString() means this Git::Repository object has not been initialized properly. A better solution is needed.
        }
    }

/// 'repo' is the repository we will clone, 'dir' is where it will be cloned in, 'origin' is for git clone --origin name
    Repository::Repository ( const Repository* repo,
                             const QUrl* dir,
                             const CloneFlags cf,
                             const QString& origin ) {
        initClone(repo,dir->toString(),cf,origin);
    }


/// 'repo' is the repository we will clone, 'dir' is where it will be cloned in, 'origin' is for git clone --origin name
    Repository::Repository ( const Repository* repo,
                             const QDir* dir,
                             const CloneFlags cf,
                             const QString& origin ) {
        initClone(repo,dir->absolutePath(),cf,origin);
    }


// From git-update-server-info

    const Result Repository::updateServerInfo ( UpdateServerInfoMode ) {
// TODO
    }


// From git-gc

    const Result Repository::gc ( const Repository::GarbageCollectMode m ) {
// TODO
    }


// From git-checkout

/// git checkout branch
/// \todo Should this return a Git::Repository::Branch or what?
    Repository::Branch* Repository::checkout ( const Repository::CheckOutFlags f,
            const Repository::TrackMode m ) {
// TODO
    }


/// git checkout -b branch
/// In this signature, regardless of the value of CheckOutFlags, at least
/// Git::Repository::Branch::CheckoutAndBranch (i. e. the '-b' parameter
/// to git-checkout) is always applied
/// \todo Should this return a Git::Repository::Branch or what?
    Repository::Branch* Repository::checkout ( const QString& newbranchname,
            const Repository::CheckOutFlags f,
            const Repository::TrackMode m ) {
// TODO
    }


/// git checkout [tree-ish] -- <paths>
/// Support detached heads (see see http://www.kernel.org/pub/software/scm/git/docs/git-checkout.html )
/// \todo Should this return a Git::Repository::Branch or what?
/// \todo Maybe the tree-ish signatures Git::Repository::Branch::checkout(const QString& treeish, ...) and Git::Repository::Branch::checkout( Git::Repository::Commit* c, ...) should be moved to Git::Repository::checkout(...) but for the time being, Branch is the right place
    Repository::Branch* Repository::checkout ( const QString& treeish,
            const QStringList& paths ) {
// TODO
    }


/// git checkout [commit_sha1] -- <paths>
/// This method is a convenience version of checkout( const QString& treeish, ...)
/// Support detached heads (see see http://www.kernel.org/pub/software/scm/git/docs/git-checkout.html )
/// \todo Should this return a Git::Repository::Branch or what?
/// \todo Maybe the tree-ish signatures Git::Repository::Branch::checkout(const QString& treeish, ...) and Git::Repository::Branch::checkout( Git::Repository::Commit* c, ...) should be moved to Git::Repository::checkout(...) but for the time being, Branch is the right place
    Repository::Branch* Repository::checkout ( Git::Repository::Commit* c,
            const QStringList& paths ) {
// TODO
    }


// From git-clean

    bool Repository::clean ( QList<QUrl> paths,
                             Repository::CleanFlags cf,
                             Repository::CleanRulesMode rm ) {
// TODO
    }


    bool Repository::clean ( QList<QFile> paths,
                             Repository::CleanFlags cf,
                             Repository::CleanRulesMode rm ) {
// TODO
    }


// From git-push


    /* This method is equivalent to:
      *   git push [--all | --mirror] [--dry-run] [--tags]
      *            [--receive-pack=<git-receive-pack>]  [-f | --force]
      *            [-v | --verbose] [<repository> <refspec>...]
      *
      * The method equivalent to:
      *   git push [--all | --mirror] [--dry-run] [--tags]
      *            [--receive-pack=<git-receive-pack>] [--repo=<repository>]
      *            [-f | --force] [-v | --verbose]
      * is in the Git::Repository::Branch class, as it has effect on a branch
      * by default, or in a repository if the local branch is not tracking a remote branch
      */
    const Result Repository::push ( const Repository* repository,
                              const QStringList& refspecs,
                              RefPushMode rpm,
                              PushFlags pf,
                              PushThinMode ptm,
                              const QString& git_receive_pack ) {
// TODO
    }


// From git-fetch

    const Result Repository::fetch ( const QStringList& refspecs,
                               FetchFlags ff,
                               FetchTagsMode ftm,
                               const QString& git_upload_pack,
                               const quint32 depth ) {
// TODO
    }

    const Result Repository::fetch ( Repository* repo,
                               const QStringList& refspecs,
                               FetchFlags ff,
                               FetchTagsMode ftm,
                               const QString& git_upload_pack,
                               const quint32 depth ) {
// TODO
    }

// From git-pull

// The parameters in this method are ordered logically: as git-pull = git-fetch + git-merge, we first specify the git-fetch-specific parameters, then the git-merge-specific parameters

    const Result Repository::pull ( const PullMode pm,
                              const QStringList& refspecs,
                              FetchFlags ff,
                              FetchTagsMode ftm,
                              const QString& git_upload_pack,
                              const quint32 depth,
                              MergeStrategy strategy,
                              LogCommitDescriptionMode lcd,
                              FastForwardMergeMode m,
                              ReturnDiffStatMode s,
                              SquashMergeMode sm ) {
// TODO
    }


    const Result Repository::pull ( Repository* repo,
                              const PullMode pm,
                              const QStringList& refspecs,
                              FetchFlags ff,
                              FetchTagsMode ftm,
                              const QString& git_upload_pack,
                              const quint32 depth,
                              MergeStrategy strategy,
                              LogCommitDescriptionMode lcd,
                              FastForwardMergeMode ffmm,
                              ReturnDiffStatMode s,
                              SquashMergeMode sm ) {
// TODO
    }


/// Uses 'git symbolic-ref HEAD' to return the current (checked-out) branch in this repository
    Repository::Branch* Repository::currentBranch() {
        // Run 'git init args'
        QProcess gitp;
        gitp.setWorkingDirectory ( m_path );
        gitp.start ( QLatin1String ( "git" ), QStringList() << QLatin1String ( "symbolic-ref" ) << QLatin1String ( "HEAD" ) );
        if ( !gitp.waitForStarted() ) {
            return 0;
            // return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "Cannot start git command line utility" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( !gitp.waitForFinished() ) {
            return 0;
            //return Git::ErrorResult ( 128, QStringList() << QLatin1String ( "The git command line utility blocked" ) ); // Using 128 because 'git' itself returns 128 for fatal errors but it might be sensible to use a different value
        }

        if ( gitp.exitCode() != 0 ) {
            return 0;
            // return Git::ErrorResult ( gitp.exitCode(), QStringList() << QString ( gitp.readAllStandardError() ) );
        }

        QString out ( gitp.readAllStandardError() );
        if ( !out.isEmpty() ) {
            return new Git::Repository::Branch ( out.section ( '/', 2, 2 ), this );
        }

        // If we arrived here, we got no output from git symbolic-ref HEAD. Let's return a null pointer in that case.
        return NULL;
    }

};
