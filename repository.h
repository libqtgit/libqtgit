/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_REPOSITORY_
#define _H_REPOSITORY_

#include <QObject>
#include <QUrl>
#include <QDir>
#include <QFile>
#include <QString>
#include <QStringList>

namespace Git {

    class Result;

    /**
     * \brief A class representing a git repository (i. e. a directory with a
     * .git file if a normal repository, or a directory.git if a bare repository)
     */
    class Repository : public QObject {

            Q_OBJECT

        private:
            QString m_path; // Path to the repository

        public:

            const QString& path() const;

            class Branch;
            class Config;
            class Commit;
            class Index;

            // From git-commit

            enum CommitFlag {
                DefaultCommitFlags = 0x0 /*< git commit */,
                OverrideIndexCommit = 0x1 /*< git commit -a */,
                SignOffCommit = 0x2 /*< git commit -s */,
                AmendCommit = 0x4 /*< git commit --amend */,
                AllowEmptyCommit = 0x8 /*< git commit --allow-empty */,
                NoVerifyCommit = 0x16 /*< git commit -n */
            };
            Q_DECLARE_FLAGS ( CommitFlags, CommitFlag )

            enum CleanUpMode {
                DefaultCleanUpBehavior = 0x0 /*< git commit */,
                VerbatimClean = 0x1 /*< git commit --cleanup=verbatim */,
                WhiteSpaceClean = 0x2 /*< git commit --cleanup=whitespace*/,
                StripClean = 0x4 /*< git commit --cleanup=strip */,
                DefaultClean = 0x8 /*< git commit --cleanup=default */
            };
            Q_ENUMS ( CleanUpMode )

            /// git commit -C commit_id -m "message"
            const Result commit ( const QString& message = QString(),
                             const Commit* c = NULL,
                             const QString& author = QString(),
                             const CommitFlags cf = DefaultCommitFlags,
                             const CleanUpMode cum = DefaultCleanUpBehavior );

            /// git commit -m "message"
            /// This is a convenience method which does the same thing as 
            /// Repository::commit( const QString& message, NULL, const QString& author, const CommitFlags cf, const CleanUpMode cum )
            const Result commit ( const QString& message = QString(),
                             const QString& author = QString(),
                             const CommitFlags cf = DefaultCommitFlags,
                             const CleanUpMode cum = DefaultCleanUpBehavior );

            /// git commit -F f -C commit_id
            /// This method takes the commit message from file 'f'
            const Result commit ( const QUrl* f,
                             const Commit* c = NULL,
                             const QString& author = QString(),
                             const CommitFlags cf = DefaultCommitFlags,
                             const CleanUpMode cum = DefaultCleanUpBehavior );

            /// Convenience method - Same as above (git commit -F f -C commit_id)
            /// This method takes the commit message from file 'f'
            const Result commit ( const QFile* f,
                             const Commit* c = NULL,
                             const QString& author = QString(),
                             const CommitFlags cf = DefaultCommitFlags,
                             const CleanUpMode cum = DefaultCleanUpBehavior );

            enum OpenFlag { /*< It is possible to combine flags. For instance "ExistingRepository | CreateRepository" means "run 'git init' in a directory where a .git repository already exists", i.e. "reinitialize" */
                DefaultOpenFlags = 0x0,
                ExistingRepository = 0x0 /*< if there is a .git directory, use it (do not create a new repository) */,
                CreateRepository = 0x1 /*< if there is no .git directory, create one (run 'git init'). Implies Default */,
                BareRepository = 0x2 /*< git init -b */
            };
            Q_DECLARE_FLAGS ( OpenFlags, OpenFlag )

            enum CloneFlag {
                DefaultCloneFlags = 0x0 /*< git clone repo directory */,
                CloneLocally = 0x1 /*< git clone -l repo directory */,
                CloneWithoutHardLinks = 0x2 /*< git clone --no-hardlinks repo directory */,
                CloneWithoutCheckOut = 0x4 /*< git clone -n repo directory */,
                BareClone = 0x8 /*< git clone -b repo directory */,
                CloneAndMirror = 0x16 /*< git clone -m repo directory  - implies Bare */
            };
            Q_DECLARE_FLAGS ( CloneFlags, CloneFlag )

            // From git-init

            /// 'dir' is where the git repository will be created. If no directory is provided, the program's working directory (".") is used.
            Repository ( const QString& dir = QString(),
                         const OpenFlags mode = Repository::DefaultOpenFlags );

            /// 'dir' is where the git repository will be created
            Repository ( const QUrl* dir,
                         const OpenFlags mode = Repository::DefaultOpenFlags );

            /// 'dir' is where the git repository will be created
            Repository ( const QDir* dir,
                         const OpenFlags mode = Repository::DefaultOpenFlags );

            // From git-clone

            /// 'repo' is the repository we will clone, 'dir' is where it will be cloned in, 'origin' is for git clone --origin name
            Repository ( const Repository* repo,
                         const QString& dir,
                         const CloneFlags cf = Repository::DefaultCloneFlags,
                         const QString& origin = "origin" );

            /// 'repo' is the repository we will clone, 'dir' is where it will be cloned in, 'origin' is for git clone --origin name
            Repository ( const Repository* repo,
                         const QUrl* dir,
                         const CloneFlags cf = Repository::DefaultCloneFlags,
                         const QString& origin = "origin" );

            /// 'repo' is the repository we will clone, 'dir' is where it will be cloned in, 'origin' is for git clone --origin name
            Repository ( const Repository* repo,
                         const QDir* dir,
                         const CloneFlags cf = Repository::DefaultCloneFlags,
                         const QString& origin = "origin" );

            // From git-update-server-info

            enum UpdateServerInfoMode {
                DefaultUpdateServerInfoBehavior = 0x0 /*< git-update-server-info */,
                ForceUpdateServerInfo = 0x1 /*< git-update-server-info --force */
            };
            Q_ENUMS ( UpdateServerInfoMode );

            const Result updateServerInfo ( UpdateServerInfoMode = DefaultUpdateServerInfoBehavior );

            // From git-gc

            enum GarbageCollectMode {
                DefaultGarbageCollectBehavior = 0x0 /*< git gc */,
                AggresiveGarbageCollect = 0x1 /*< git gc --aggressive */,
                AutoGarbageCollect = 0x2 /*< git gc --auto */
            };
            Q_ENUMS ( GargageCollectMode )

            const Result gc ( const Repository::GarbageCollectMode m );

            // From git-checkout

            enum CheckOutFlag {
                DefaultCheckoutFlags = 0x0 /*< git checkout newbranchname [branch] */,
                ForceCheckout = 0x1 /*< git checkout -f newbranchname [branch] */,
                CheckoutAndBranch = 0x2 /*< git checkout -b newbranchname [branch] */,
                CheckoutAndBuildReflog = 0x4 /*< git checkout -l newbranchname [branch] */,
                CheckoutAndMergeThreeWay = 0x8 /*< git checkout -m newbranchname [branch] */
            };
            Q_DECLARE_FLAGS ( CheckOutFlags, CheckOutFlag )

            enum TrackMode {
                DefaultTrackCheckoutBehavior = 0x0 /*< git checkout newbranchname [branch] */,
                TrackCheckout = 0x1 /*< git checkout -t newbranchname [branch] */,
                NoTrackCheckout = 0x2 /*< git checkout --no-track newbranchname [branch] */
            };
            Q_ENUMS ( TrackMode )

            /// \todo Add support for detached heads ( ) => Easies way is probably adding a "checkout" signature which receives a Git::Repository::Commit instead of a Git::Repository::Branch.
            /// \todo Should branches be received as a "QString& branchname" or as a "Git::Repository::Branch* branchptr" ???

            /// \todo 'checkout' is missing a lot of signatures!

            /// git checkout branch
            /// \todo Should this return a Git::Repository::Branch or what?
            Branch* checkout ( const Repository::CheckOutFlags f = Repository::DefaultCheckoutFlags,
                               const Repository::TrackMode m = Repository::DefaultTrackCheckoutBehavior );

            /// git checkout -b branch
            /// In this signature, regardless of the value of CheckOutFlags, at least
            /// Git::Repository::Branch::CheckoutAndBranch (i. e. the '-b' parameter
            /// to git-checkout) is always applied
            /// \todo Should this return a Git::Repository::Branch or what?
            Branch* checkout ( const QString& newbranchname,
                               const Repository::CheckOutFlags f = Repository::DefaultCheckoutFlags,
                               const Repository::TrackMode m = Repository::DefaultTrackCheckoutBehavior );

            /// git checkout [tree-ish] -- <paths>
            /// Support detached heads (see see http://www.kernel.org/pub/software/scm/git/docs/git-checkout.html )
            /// \todo Should this return a Git::Repository::Branch or what?
            /// \todo Maybe the tree-ish signatures Git::Repository::Branch::checkout(const QString& treeish, ...) and Git::Repository::Branch::checkout( Git::Repository::Commit* c, ...) should be moved to Git::Repository::checkout(...) but for the time being, Branch is the right place
            Branch* checkout ( const QString& treeish,
                               const QStringList& paths = QStringList() );

            /// git checkout [commit_sha1] -- <paths>
            /// This method is a convenience version of checkout( const QString& treeish, ...)
            /// Support detached heads (see see http://www.kernel.org/pub/software/scm/git/docs/git-checkout.html )
            /// \todo Should this return a Git::Repository::Branch or what?
            /// \todo Maybe the tree-ish signatures Git::Repository::Branch::checkout(const QString& treeish, ...) and Git::Repository::Branch::checkout( Git::Repository::Commit* c, ...) should be moved to Git::Repository::checkout(...) but for the time being, Branch is the right place
            Branch* checkout ( Git::Repository::Commit* c,
                               const QStringList& paths = QStringList() );

            // From git-clean

            enum CleanFlag {
                DefaultCleanFlags = 0x0 /*< git clean [-x | -X] <paths> */,
                CleanUntrackedDirectories = 0x1 /*< git clean -d [-x | -X] <paths> */,
                ForceClean = 0x2 /*< git clean -f [-x | -X] <paths> */
            };
            Q_DECLARE_FLAGS ( CleanFlags, CleanFlag )

            enum CleanRulesMode {
                DefaultCleanRulesBehavior = 0x0 /*< git clean [-d] [-f] <paths> */,
                IgnoreIgnoreRules = 0x1 /*< git clean [-d] [-f] -x <paths> */,
                UseIgnoreRules = 0x2 /*< git clean [-d] [-f] -X <paths> */
            };
            Q_ENUMS ( CleanRulesMode )

            bool clean ( QList<QUrl> paths = QList<QUrl>(),
                         Repository::CleanFlags cf = DefaultCleanFlags,
                         Repository::CleanRulesMode rm = Repository::DefaultCleanRulesBehavior );

            bool clean ( QList<QFile> paths = QList<QFile>(),
                         Repository::CleanFlags cf = DefaultCleanFlags,
                         Repository::CleanRulesMode rm = Repository::DefaultCleanRulesBehavior );

            // From git-push

            enum RefPushMode {
                DefaultRefPushBehavior = 0x0 /*< git-push repo */,
                MergeRefPushAll = 0x1 /*< git-push --all repo */,
                RefPushMirror = 0x2 /*< git-push --mirror repo */
            };
            Q_ENUMS ( RefPushMode )

            enum PushFlag {
                DefaultPushFlags = 0x0 /*< git-push repo */,
                PushTags = 0x1 /*< git-push --tags repo */,
                ForcePush = 0x2 /*< git-push --force repo */
            };
            Q_DECLARE_FLAGS ( PushFlags, PushFlag )

            enum PushThinMode {
                DefaultPushThinBehavior = 0x0 /*< git-push repo */,
                PushThin = 0x1 /*< git-push --thin repo */,
                NoPushThin = 0x2 /*< git-push --no-thin repo */
            };
            Q_ENUMS ( PushThinMode )


            /* This method is equivalent to:
             *   git push [--all | --mirror] [--dry-run] [--tags]
             *            [--receive-pack=<git-receive-pack>]  [-f | --force]
             *            [-v | --verbose] [<repository> <refspec>...]
             *
             * The method equivalent to:
             *   git push [--all | --mirror] [--dry-run] [--tags]
             *            [--receive-pack=<git-receive-pack>] [--repo=<repository>]
             *            [-f | --force] [-v | --verbose]
             * is in the Git::Repository::Branch class, as it has effect on a branch
             * by default, or in a repository if the local branch is not tracking a remote branch
             */
            const Result push ( const Repository* repository,
                          const QStringList& refspecs = QStringList(),
                          RefPushMode rpm = DefaultRefPushBehavior,
                          PushFlags pf = DefaultPushFlags,
                          PushThinMode ptm = DefaultPushThinBehavior,
                          const QString& git_receive_pack = QString() );

            // From git-fetch

            enum FetchFlag {
                DefaultFetchFlags = 0x0 /*< git-fetch repository refspec */,
                AppendFetched = 0x1 /*< git-fetch -a repository refspec */,
                ForceFetch = 0x2 /*< git-fetch -f repository refspec */,
                KeepFetchedPack = 0x4 /*< git-fetch -k repository refspec */
            };
            Q_DECLARE_FLAGS ( FetchFlags, FetchFlag )

            enum FetchTagsMode {
                DefaultFetchTagsBehavior = 0x0 /*< git-fetch repository refspec */,
                NoFetchTags = 0x1 /*< git-fetch -n repository refspec */,
                FetchTags = 0x2 /*< git-fetch -t repository refspec */
            };
            Q_ENUMS ( FetchTagsMode )

            const Result fetch ( /*< By default, fetch from the repository specified in [remote "origin"] in .git/config -- It is not possible to use 'Repository* repo = this' because we cannot use a local variable as a default parameter (see http://www.daniweb.com/forums/thread120058.html ) */
                const QStringList& refspecs = QStringList() /*< If calling 'Repository::fetch' without a repository (i. e. default to what is specified in [remote 'origin'] in .git/config) , no need to specify refspecs because they are already specified in "[remote 'origin'] -> fetch = ..." */,
                FetchFlags ff = DefaultFetchFlags,
                FetchTagsMode ftm = DefaultFetchTagsBehavior,
                const QString& git_upload_pack = QString(),
                const quint32 depth = 0 ); /*< Empirically I found depth = 0 is the same as not specifying any depth */

            const Result fetch ( Repository* repo,
                           const QStringList& refspecs = QStringList(), /*< If calling 'Repository::fetch' without a repository (i. e. default to what is specified in [remote 'origin'] in .git/config) , no need to specify refspecs because they are already specified in "[remote 'origin'] -> fetch = ..." */
                           FetchFlags ff = DefaultFetchFlags,
                           FetchTagsMode ftm = DefaultFetchTagsBehavior,
                           const QString& git_upload_pack = QString(),
                           const quint32 depth = 0 ); /*< Empirically I found depth = 0 is the same as not specifying any depth */

            // From git-archive

            // archive();

            // From git-pull

            // The parameters in this method are ordered logically: as git-pull = git-fetch + git-merge, we first specify the git-fetch-specific parameters, then the git-merge-specific parameters

            enum PullMode {
                DefaultPullBehavior = 0x0 /*< 'git pull' in git-pull, 'git rebase' in git-rebase */,
                MergePull = 0x1 /*< 'git pull --no-rebase' in git-pull, 'git rebase --merge' in git-rebase */,
                RebasePull = 0x2 /*< 'git pull --rebase' in git-pull, 'git rebase' in git-rebase */,
            };
            Q_ENUMS ( PullMode )

            // FIXME WARNING! THIS CODE IS DUPLICATED FROM Git::Repository::Branch
            // From git-merge

            enum MergeStrategy {
                ResolveMergeStrategy = 0x0 /*< git-merge -s resolve */,
                RecursiveMergeStrategy = 0x1 /*< git-merge -s recursive */,
                OctopusMergeStrategy = 0x2 /*< git-merge -s octopus */,
                OursMergeStrategy = 0x4 /*< git-merge -s ours */,
                SubtreeMergeStrategy = 0x8 /*< git-merge -s subtree */
            };
            Q_ENUMS ( MergeStrategy )

            enum ReturnDiffStatMode {
                DefaultDiffStatBehavior = 0x0 /*< git-merge */,
                NoDiffStat = 0x1 /*< git-merge -n */,
                ReturnDiffStat = 0x2 /*< git-merge --stat */
            };
            Q_ENUMS ( ReturnMergeStatMode )

            enum LogCommitDescriptionMode {
                DefaultLogCommitDescriptionBehavior = 0x0 /*< git-merge */,
                NoLogCommitDescription = 0x1 /*< git-merge --no-log */,
                LogCommitDescription = 0x2 /*< git-merge --log */
            };
            Q_ENUMS ( LogCommitDescriptionMode )

            enum FastForwardMergeMode {
                DefaultMergeFastForwardBehavior = 0x0 /*< git-merge */,
                NoMergeFastForward = 0x1 /*< git-merge --no-ff */,
                MergeFastForward = 0x2 /*< git-merge --ff */
            };
            Q_ENUMS ( FastForwardMergeMode )

            enum SquashMergeMode {
                DefaultSquashMergeBehavior = 0x0 /*< git-merge */,
                SquashMerge = 0x1 /*< git-merge --squash */,
                NoSquashMerge = 0x2 /*< git-merge --no-squash */
            };
            Q_ENUMS ( SquashMergeMode )
            // END OF DUPLICATED CODE FROM Git::Repository::Branch

            const Result pull ( /*< By default, pull from the repository specified in [remote "origin"] in .git/config -- It is not possible to use 'Repository* repo = this' because we cannot use a local variable as a default parameter (see http://www.daniweb.com/forums/thread120058.html ) */
                const PullMode pm = DefaultPullBehavior,
                const QStringList& refspecs = QStringList(), /*< If calling 'Repository::pull' without a repository (i. e. default to what is specified in [remote 'origin'] in .git/config) , no need to specify refspecs because they are already specified in "[remote 'origin'] -> fetch = ..." */
                FetchFlags ff = DefaultFetchFlags,
                FetchTagsMode ftm = DefaultFetchTagsBehavior,
                const QString& git_upload_pack = QString(),
                const quint32 depth = 0, /*< Empirically I found depth = 0 is the same as not specifying any depth */
                MergeStrategy strategy = RecursiveMergeStrategy,
                LogCommitDescriptionMode lcd = DefaultLogCommitDescriptionBehavior,
                FastForwardMergeMode ffmm = DefaultMergeFastForwardBehavior,
                ReturnDiffStatMode s = DefaultDiffStatBehavior,
                SquashMergeMode sm = DefaultSquashMergeBehavior );

            const Result pull ( Repository* repo,
                          const PullMode = DefaultPullBehavior,
                          const QStringList& refspecs = QStringList(), /*< If calling 'Repository::pull' without a repository (i. e. default to what is specified in [remote 'origin'] in .git/config) , no need to specify refspecs because they are already specified in "[remote 'origin'] -> fetch = ..." */
                          FetchFlags ff = DefaultFetchFlags,
                          FetchTagsMode ftm = DefaultFetchTagsBehavior,
                          const QString& git_upload_pack = QString(),
                          const quint32 depth = 0, /*< Empirically I found depth = 0 is the same as not specifying any depth */
                          MergeStrategy strategy = RecursiveMergeStrategy,
                          LogCommitDescriptionMode lcd = DefaultLogCommitDescriptionBehavior,
                          FastForwardMergeMode ffmm = DefaultMergeFastForwardBehavior,
                          ReturnDiffStatMode s = DefaultDiffStatBehavior,
                          SquashMergeMode sm = DefaultSquashMergeBehavior );

            /// Uses 'git symbolic-ref HEAD' to return the current (checked-out) branch in this repository
            Git::Repository::Branch* currentBranch();
        private:
            enum CommitMode {
                StringMessage,
                FileMessage
            };
            const Result commit(CommitMode mode,
                                const QFile* file,
                                const QString& message,
                                const Commit* c,
                                const QString& author,
                                const CommitFlags cf,
                                const CleanUpMode cum);
            void initCreate(const QString& arg1, OpenFlags arg2);
            void initClone(const Repository* repo,
                                       const QString& dir,
                                       const CloneFlags cf,
                                       const QString& origin);

    };
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::CommitFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::OpenFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::CloneFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::CheckOutFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::CleanFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::PushFlags )
    Q_DECLARE_OPERATORS_FOR_FLAGS ( Repository::FetchFlags )
}

#endif
