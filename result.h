/*
   This file is part of the the QtGit library

   Copyright (c) 2008, 2009 Pau Garcia i Quiles <pgquiles@elpauer.org>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License version 2.1 or version 3 as published by the Free
   Software Foundation.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LIB.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

*/

#ifndef _H_RESULT_
#define _H_RESULT_

//#include <QObject>
#include <QStringList>

namespace Git {

    /**
     * \brief The result of an operation (i. e. the result of executing a method).
     *
     * Most methods return a Git::Result, for instance a commit, a revert,
     * adding a file to the index, etc. The result of an operation might be a
     * conflict, a diffstat, etc
     *
     * When developing, you should work with Git::Result more or less like you
     * do with QEvent. Instead of "event filters", we have "result filters" but
     * the idea is the same: check Git::Result::type() and cast accordingly.
     *
     * \todo Most methods which return "bool" in Git::Repository,
     * Git::Repository::Branch, etc should actually return a Git::Result
     * and set type and message.
     * \internal In order to use setMessage, 'friend' declarations will be needed.
     *
     */
    class Result {

            Q_GADGET

        public:
            enum ResultType {
                Information = 0x0, /*< No error, everything went fine */
                Diffstat = 0x1, /*< Diffstat */
                Warning = 0x2, /*< No error but there are warnings */
                Conflict = 0x4, /*< There's a conflict */
                Error = 0x8 /*< Something caused an error */
            };
            Q_ENUMS ( ResultType )

        private:
            int m_errorCode;
            ResultType m_type;
            QStringList m_stdErrMessage;
            QStringList m_stdOutMessage;

            virtual void setStdErrMessage ( const QStringList& errMessage );
            virtual void setStdOutMessage ( const QStringList& outMessage );
            virtual void setType ( const ResultType );
            virtual void setErrorCode ( int error );

        public:
            Result(); /// If you use this constructor, you must call setErrorCode, setType and setMessage to construct a valid Result object
            Result ( int errorcode, const ResultType type, const QStringList& stderrMessage, const QStringList& stdoutMessage = QStringList() ); /// Convenience constructor

            const int errorCode() const {
                return m_errorCode;
            }; // Return the errorCode
            const ResultType type() const {
                return m_type;
            } ;
            const QStringList stderrMessage() const {
                return m_stdErrMessage;
            };
            const QStringList stdoutMessage() const {
                return m_stdOutMessage;
            };
    };

    class InformativeResult : public Result {

            Q_GADGET

        private:
            void setStdErrMessage ( const QStringList& stdErrMessage );
            void setStdOutMessage ( const QStringList& stdOutMessage );
            void setType ( const ResultType );
            void setErrorCode ( int error );

        public:
            InformativeResult();
            InformativeResult ( int errorcode, const QStringList& stderrMessage, const QStringList& stdOutMessage = QStringList() ); /// Convenience constructor

    };

    class DiffStatResult : public Result {

            Q_GADGET

        private:
            void setStdErrMessage ( const QStringList& stdErrMessage );
            void setStdOutMessage ( const QStringList& stdOutMessage );
            void setType ( const ResultType );
            void setErrorCode ( int error );

        public:
            DiffStatResult();
            DiffStatResult ( int errorcode, const QStringList& stderrMessage, const QStringList& stdOutMessage = QStringList() ); /// Convenience constructor

    };

    class WarningResult : public Result {

            Q_GADGET

        private:
            void setStdErrMessage ( const QStringList& stdErrMessage );
            void setStdOutMessage ( const QStringList& stdOutMessage );
            void setType ( const ResultType );
            void setErrorCode ( int error );

        public:
            WarningResult();
            WarningResult ( int errorcode, const QStringList& stderrMessage, const QStringList& stdOutMessage = QStringList() ); /// Convenience constructor

    };

    class ConflictResult : public Result {

            Q_GADGET

        private:
            void setStdErrMessage ( const QStringList& stdErrMessage );
            void setStdOutMessage ( const QStringList& stdOutMessage );
            void setType ( const ResultType );
            void setErrorCode ( int error );

        public:
            ConflictResult();
            ConflictResult ( int errorcode, const QStringList& stderrMessage, const QStringList& stdOutMessage = QStringList() ); /// Convenience constructor

    };

    class ErrorResult : public Result {

            Q_GADGET

        private:
            void setStdErrMessage ( const QStringList& stdErrMessage );
            void setStdOutMessage ( const QStringList& stdOutMessage );
            void setType ( const ResultType );
            void setErrorCode ( int error );

        public:
            ErrorResult();
            ErrorResult ( int errorcode, const QStringList& stderrMessage, const QStringList& stdOutMessage = QStringList() ); /// Convenience constructor

    };

}

#endif
