#include <iostream>

class A {

  private:
    std::string m_theAString;

  public:
    A(std::string s);

  class B {
    private:
      std::string m_theBString;

    public:
      B(std::string s);
  };

};

A::A(std::string s) : m_theAString(s) {
  std::cerr << "In A::A(std::string s), m_theAString = " << s << "\n"; 
} 

A::B::B(std::string s) : m_theBString(s) {
  std::cerr << "In A::B::B(std::string s), m_theBString = " << s << "\n"; 
} 

int main( int argc, char* argv[] ) {
  A("hola");
  A::B("blah");
}
